# Sejam Bem-Vindo!

## Olá,sou <strong>Henrique Vieira!</strong>

<img align="left" width="5%" src="https://gitlab.com/uploads/-/system/user/avatar/1649606/avatar.png?width=400"/>

Tenho doutorado em engenharia na área de mecatrônica formado pela UNICAMP, atualmente atuo em projetos voltado para o mercado financeiro, principalmente relacionado a Câmbio e Bolsa de Valores.
Tenho mais de 5 anos em experiência de docência, nos quais quatro foram para o Ensino Superior dos cursos de Engenharia (Elétrica, Computação, Automação e Controle).

---

### ⚠️ Informação Geral

> Este repositório é gratuito e pessoal com variários exemplos de códigos feitos em Python que pode ser útil para você. Caso algum código tenha sido válido para você, e queria agradeçer pode enviar um [PIX 💰](https://nubank.com.br/pagar/1oeg8/KqfUsds7v9) no valor que desejar. O objetivo do repositório é incluir cada vez mais códigos.

---

## ⭐ Rede Sociais

[![Linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/henrique-s-vieira/)
[![Instagram](https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white)](https://www.instagram.com/hdsvieira/)
[![GitLab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/hdsvieira)

### 🚀 Linguagem Utilizada no Repositório

[![Python](https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white)](https://www.python.org/)